# :avocado: Avocadostore - nachhaltig

- [BagBase - Kurierrucksack BG286 Recycelter Roll-Top](https://www.avocadostore.de/products/276276-kurierrucksack-bg286-recycelter-roll-top-rucksack-bagbase?variant_id=2278463)
- [2-in-1 Rucksack aus Kork und Bio-Baumwolle](https://www.avocadostore.de/products/230611-triple-2-in-1-rucksack-aus-kork-und-bio-baumwolle-sperling?variant_id=1830133)
- [Leichter Tyvek©-Rucksack mit Rollverschluss Nikita](https://www.avocadostore.de/products/224327-leichter-tyvek-c-rucksack-mit-rollverschluss-nikita-schwarz-strich-grau-morethanhip)
- [Rucksack aus Papier, robust, wasserfest & vegan Rolltop](https://www.avocadostore.de/products/146915-rucksack-aus-papier-robust-wasserfest-und-vegan-rolltop-papero?variant_id=1527613)
- [Fahrradtasche/Rucksack 2in1](https://www.avocadostore.de/products/209650-fahrradtasche-strich-rucksack-2in1-mitienda-shop?variant_id=2026867)
- [Raja Lite Ecopack 30](https://www.avocadostore.de/products/196571-raja-lite-ecopack-30-ethnotek?variant_id=1526093)
- [Kurierrucksack CIRCLE](https://www.avocadostore.de/products/190525-kurierrucksack-circle-halfar?variant_id=1474252)
- [Rolltop Rucksack aus recycelten Zementsäcken - Tantor](https://www.avocadostore.de/products/341806-rolltop-rucksack-aus-recycelten-zementsaecken-tantor-morethanhip?variant_id=2933030)
- [Rollrucksack aus LKW-Plane in 11 Farben/3 Größen](https://www.avocadostore.de/products/201349-rollrucksack-aus-lkw-plane-in-11-farben-strich-3-groessen-leonca?variant_id=2641713)
- [Daily Rucksack aus Kork und Bio-Baumwolle](https://www.avocadostore.de/products/184814-daily-rucksack-aus-kork-und-bio-baumwolle-sperling?variant_id=1649713)
- [Waterproof Roll-Top Rucksack aus Kork](https://www.avocadostore.de/products/212374-waterproof-roll-top-rucksack-aus-kork-simaru?variant_id=1657975)

# :house: Etuis-Mertl - vor Ort
- [VAUDE CityGo 23, baltic sea](https://etuis-mertl.bagmondo.de/products/citygo-23-baltic-sea-6ecefb)
- [OAK25 Laptoprucksack Reflective Rolltop Bk-Gy Black](https://etuis-mertl.bagmondo.de/products/freizeitrucksack-reflective-rolltop-bk-black-43adae)
- [Nitro Freizeitrucksack 1181-878065 Black Rose](https://etuis-mertl.bagmondo.de/products/freizeitrucksack-1181-878065-black-rose-2688a2)
- [ZWEI Freizeitrucksack Benno Ink](https://etuis-mertl.bagmondo.de/products/benno-be200-benno-ink-c6fea8)

# :handbag: Andere Ideen
- [New Rebels -  Mart - Rolltop - Rucksack - Rainbow - Regenbogen - Large II](https://www.new-rebels.com/de/new-rebels-mart-roll-top-backpack-rainbow-large-ii.html?channable=044d4669640032363131313338313716&msclkid=4303a5513e8a10dae429c6c8d713dcd3)


:computer: https://gitlab.com/Enesma/publee/-/blob/main/RucksackSuche.md
